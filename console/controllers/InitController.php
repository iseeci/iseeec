<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\db\Connection;
use frontend\modules\user\models\ConsoleUser;
use dektrium\user\Finder;
use dektrium\user\models\Token;
use frontend\models\DireccionParametro;

class InitController extends Controller
{
	public $admin_id=1, $ingeniero1_id=3, $ingeniero2_id=4, $ingeniero3_id=5, $manager1_id=6, $manager2_id=7;
	public $colombia_id=1, $antioquia_id=1, $medellin_id=1, $cundinamarca_id=2, $bogota_id=2, $valle_id=3, $cali_id=3;
	
	public function actionFull(){
		$this->actionUsersRbac();
	}
	
	public function actionFullDummy(){
		$this->actionFull();
	}
	
	public function actionUsersRbac(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tablas de usuarios'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;
			truncate token;truncate social_account;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate social_account;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate profile;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate user;foreign_key_checks=1;')->execute();
		
		echo 'Registrando administrador'.PHP_EOL;
		$fernandrez= new ConsoleUser(['scenario'=>'register']);
		$fernandrez->setAttributes([
            'email'    => 'fernandrez@gmail.com',
            'username' => 'fernandrez',
            'password' => 'fernandrez2015'
        ]);
		$fernandrez->register();
		$fernandrez->attemptConfirmation($fernandrez->code);
		
		echo 'Inicializando tablas de autenticacion'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate auth_item_child;
			truncate auth_assignment;
			truncate auth_item;
			truncate auth_rule;
			foreign_key_checks=1;')->execute();
        $auth = Yii::$app->authManager;
		
		echo 'Creando permisos'.PHP_EOL;
        //Controladores y sus acciones
		$actions['base']=['index','view'];
        $actions['geo']['pais']=['update','create','delete'];
        $actions['geo']['region']=['update','create','delete','get-regiones-pais'];
        $actions['geo']['ciudad']=['update','create','delete','get-ciudades-region'];
        $actions['corte']['lamina']=['create','update','delete','create-batch','row'];
        
        $modules=array_filter(array_keys($actions),function($v){return $v!='base';});
		
		foreach($modules as $m){
			$controllers=array_keys($actions[$m]);
			foreach($controllers as $c){
				$actions[$m][$c]=array_merge($actions['base'],isset($actions[$m][$c])?$actions[$m][$c]:[]);
			}
		}
		
        $fcp='full-controller-permission';
		foreach($modules as $m){
			$controllers=array_keys($actions[$m]);
			foreach($controllers as $c){
				$permissions[$m][$c][$fcp]=$auth->createPermission($m.'-'.$c.'-'.$fcp);
				$permissions[$m][$c][$fcp]->description = $m.'-'.$c.'-'.$fcp;
				$auth->add($permissions[$m][$c][$fcp]);
				foreach($actions[$m][$c] as $a){
					$permissions[$m][$c][$a]=$auth->createPermission($m.'-'.$c.'-'.$a);
					$permissions[$m][$c][$a]->description = $m.'-'.$c.'-'.$a;
					$auth->add($permissions[$m][$c][$a]);
					$auth->addChild($permissions[$m][$c][$fcp],$permissions[$m][$c][$a]);
				}
			}		
		}		
		
		echo 'Creando roles'.PHP_EOL;
        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);
		
		echo 'Asignando permisos a rol admin'.PHP_EOL;
        foreach($permissions as $m=>$permission){
        	foreach($permission as $c=>$p){
        		$auth->addChild($adminRole, $p[$fcp]);
			}
		}
		
		echo 'Asignando roles a usuarios'.PHP_EOL;
		//Admin
        $auth->assign($adminRole, $fernandrez->id);$this->admin_id=$fernandrez->id;
    }

	public function actionAppPars(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tabla de parametros de aplicacion'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate parametro;foreign_key_checks=1;')->execute();
		$fields=[];
		$data=[];
		//$this->batchInsert('frontend\models\Parametro',$fields,$data);
	}

	private function batchInsert($model,$fields,$data){
		$ret=[];
		foreach($data as $d){
			$m=new $model;$setA=[];	
			foreach($fields as $k=>$f){
				$setA[$f]=$d[$k];
			}
			$m->setAttributes($setA);
			$m->detachBehavior('blameable');
			$m->save();
			if(count($m->errors)>0){
				var_dump($m->errors);
				var_dump($m->attributes);die;
			}
			$ret[]=$m->id;
		}
		return $ret; 
	}
}