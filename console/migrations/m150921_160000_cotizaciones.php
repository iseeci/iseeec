<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_160000_cotizaciones extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%cotizacion}}', [
            'id' => $this->primaryKey(),
			'nombre' => $this->string()->notNull(),
			'telefono' => $this->string()->notNull(),
			'email' => $this->string()->notNull(),
			'sector' => $this->string()->notNull(),
			'numero_categorias' => $this->integer()->notNull(),
			'numero_productos' => $this->integer()->notNull(),
			'numero_atributos' => $this->integer()->notNull(),
			'numero_imagenes' => $this->integer()->notNull(),
			'numero_correos' => $this->integer()->notNull(),
			'seo_avanzado' => $this->boolean()->notNull(),
			'numero_redes_sociales' => $this->integer()->notNull(),
			'numero_usuarios' => $this->integer()->notNull(),
			'parametros_desarrollo' => $this->string(500)->notNull(),
			'costo_desarrollo' => $this->float()->notNull(),
			'horas_desarrollo' => $this->float()->notNull(),
			'parametros_funcionamiento' => $this->string(500)->notNull(),
			'costo_funcionamiento_mensual' => $this->float()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('inicial')
        ], $tableOptions);
		
		$this->addForeignKey('fk_cotizacion_created_by','{{%cotizacion}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_cotizacion_updated_by','{{%cotizacion}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%cotizacion}}');
    }
}
