$('[id^=carousel-selector-]').click( function(){
  var id_selector = $(this).attr("id");
  var id = id_selector.substr(id_selector.length -1);
  id = parseInt(id);
  $('#social').carousel(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $(this).addClass('selected');
});
$('#social').on('slid.bs.carousel', function (e) {
  var id = $('#social').children('.carousel-inner').children('.item.active').data('slide-number');
  id = parseInt(id);
  $('[id^=carousel-selector-on]').hide();
  $('[id^=carousel-selector-off]').show();
  $('[id=carousel-selector-off-'+id+']').hide();
  $('[id=carousel-selector-on-'+id+']').show();
});