<?php

namespace frontend\modules\trx;

class Trx extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\trx\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
