<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trx\models\Cotizacion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'telefono',
            'email:email',
            'sector',
            'numero_categorias',
            'numero_productos',
            'numero_atributos',
            'numero_imagenes',
            'numero_correos',
            'seo_avanzado',
            'numero_redes_sociales',
            'numero_usuarios',
            'parametros_desarrollo',
            'costo_desarrollo',
            'horas_desarrollo',
            'parametros_funcionamiento',
            'costo_funcionamiento_mensual',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'status',
        ],
    ]) ?>

</div>
