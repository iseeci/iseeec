<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trx\models\Cotizacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form -> errorSummary($model); ?>

	<div class="row">
		<div class="top-field-container col-sm-6">
			<?= $form -> field($model, 'nombre') -> textInput(['maxlength' => true]) ?>
		</div>
		<div class="top-field-container col-sm-4 pull-right">
			<?= $form -> field($model, 'telefono') -> textInput(['maxlength' => true]) ?>
		</div>
	</div>

	<div class="row">
		<div class="top-field-container col-sm-6">
			<?= $form -> field($model, 'email') -> textInput(['maxlength' => true]) ?>
		</div>
		<div class="top-field-container col-sm-4 pull-right">
			<?= $form -> field($model, 'sector') -> textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row-fluid top-buffer-slider">
		<div class="col-sm-3">
			<?= Html::activeLabel($model, 'numero_categorias') ?>
			<?= TouchSpin::widget(['model' => $model, 'attribute' => 'numero_categorias', 'options' => ['value' => 5,'class'=>'text-center'], 'pluginOptions' => ['min' => 1, 'max' => 30, 'step' => 1]]); ?>
		</div>
		<div class="col-sm-3">
			<?= Html::activeLabel($model, 'numero_atributos') ?>
			<?= TouchSpin::widget(['model' => $model, 'attribute' => 'numero_atributos', 'options' => ['value' => 10,'class'=>'text-center'], 'pluginOptions' => ['min' => 1, 'max' => 100, 'step' => 1]]); ?>
		</div>
		<div class="col-sm-3">
			<?= Html::activeLabel($model, 'numero_productos') ?>
			<?= TouchSpin::widget(['model' => $model, 'attribute' => 'numero_productos', 'options' => ['value' => 150,'class'=>'text-center'], 'pluginOptions' => ['min' => 1, 'max' => 2500, 'step' => 1]]); ?>
		</div>
		<div class="col-sm-3">
			<?= Html::activeLabel($model, 'numero_imagenes') ?>
			<?= TouchSpin::widget(['model' => $model, 'attribute' => 'numero_imagenes', 'options' => ['value' => 150,'class'=>'text-center'], 'pluginOptions' => ['min' => 1, 'max' => 10000, 'step' => 1]]); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row-fluid top-buffer-slider">
		<div class="col-sm-3">
			<?= Html::activeLabel($model, 'numero_correos') ?>
			<?= TouchSpin::widget(['model' => $model, 'attribute' => 'numero_correos', 'options' => ['value' => 3,'class'=>'text-center'], 'pluginOptions' => ['min' => 1, 'max' => 30, 'step' => 1]]); ?>
		</div>
		<div class="col-sm-3">
			<?= Html::activeLabel($model, 'numero_redes_sociales') ?>
			<?= TouchSpin::widget(['model' => $model, 'attribute' => 'numero_redes_sociales', 'options' => ['value' => 2,'class'=>'text-center'], 'pluginOptions' => ['min' => 1, 'max' => 8, 'step' => 1]]); ?>
		</div>
		<div class="col-sm-3">
			<?= Html::activeLabel($model, 'numero_usuarios') ?>
			<?= TouchSpin::widget(['model' => $model, 'attribute' => 'numero_usuarios', 'options' => ['value' => 150,'class'=>'text-center'], 'pluginOptions' => ['min' => 1, 'max' => 2500, 'step' => 1]]); ?>
		</div>
		<div class="col-sm-3">
			<?= $form -> field($model, 'seo_avanzado') -> checkBox() ?>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="row-fluid form-group top-buffer-slider">
		<?= Html::submitButton(Yii::t('app', 'COTIZAR'), ['id' => 'boton-cotizar', 'class' => 'btn btn-cotizar pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
