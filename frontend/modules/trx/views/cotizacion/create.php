<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\trx\models\Cotizacion */

/*
$this->params['breadcrumbs'][] = ['label' => 'Cotizacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
$this->title = 'Cotizar E-Commerce';
$this->registerMetaTag(['content'=>\Yii::t('app','e-commerce, iseeec, iseeci, cotizacion, imagenes, categorias'),'name'=>'keywords']);
$this->registerMetaTag(['content'=>\Yii::t('app','With proper categories, products, number of images and other key store details, iSeeEC performs Dev and Maintain Budget'),'name'=>'description']);
?>
<div class="cotizacion-crear container">
	<div class="calculator-container"><?= Html::img('@web/images/cotizacion/calculator.png')?></div>
    <h1><?= Html::encode(strtoupper($this->title)) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
