<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trx\models\Cotizacion */

$this->title = \Yii::t('app','Cotización #').$model->id.' '.\Yii::t('app','creada');
/*$this->params['breadcrumbs'][] = ['label' => 'Cotizacion'];
$this->params['breadcrumbs'][] = $model->id;*/
?>
<div class="container container-gracias text-center">
	<h1><?= $this->title; ?></h1>
<?= $model->nombre ?> hemos enviado a tu correo la cotización solicitada. Esperamos saber de tí muy pronto, no dudes en contactarnos.
<br>
<br>
GRACIAS
</div>
<br>
<br>
<div class="text-center">
<?= Html::img('@web/images/site/logo-home-iseeec.png',['height'=>'100px']); ?>
</div>