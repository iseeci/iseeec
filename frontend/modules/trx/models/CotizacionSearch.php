<?php

namespace frontend\modules\trx\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\trx\models\Cotizacion;

/**
 * CotizacionSearch represents the model behind the search form about `frontend\modules\trx\models\Cotizacion`.
 */
class CotizacionSearch extends Cotizacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'numero_imagenes', 'numero_productos', 'numero_categorias', 'numero_atributos', 'numero_usuarios', 'created_by', 'updated_by'], 'integer'],
            [['nombre', 'telefono', 'email', 'sector', 'created_at', 'updated_at', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cotizacion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'numero_imagenes' => $this->numero_imagenes,
            'numero_productos' => $this->numero_productos,
            'numero_categorias' => $this->numero_categorias,
            'numero_atributos' => $this->numero_atributos,
            'numero_usuarios' => $this->numero_usuarios,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'sector', $this->sector])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
