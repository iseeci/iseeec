<?php

namespace frontend\modules\trx\models;

use Yii;
use dektrium\user\models\User;
use frontend\components\securitybehaviors\StripTagsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * This is the model class for table "cotizacion".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $telefono
 * @property string $email
 * @property string $sector
 * @property integer $numero_categorias
 * @property integer $numero_productos
 * @property integer $numero_atributos
 * @property integer $numero_imagenes
 * @property integer $numero_correos
 * @property integer $seo_avanzado
 * @property integer $numero_redes_sociales
 * @property integer $numero_usuarios
 * @property string $parametros_desarrollo
 * @property double $costo_desarrollo
 * @property double $horas_desarrollo
 * @property string $parametros_funcionamiento
 * @property double $costo_funcionamiento_mensual
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $status
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Cotizacion extends \yii\db\ActiveRecord
{
	public $desarrollo=[
		'q'=>['numero_imagenes'=>1, 'numero_correos'=>1, 'numero_productos'=>0.7, 'numero_categorias'=>0.4, 'numero_atributos'=>0.4, 'numero_redes_sociales'=>0.2],
		'c'=>[8500,1.5]];
	public $funcionamiento=[
		'q'=>['numero_usuarios'=>0.001, 'numero_redes_sociales'=>1],
		'c'=>[200000]];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cotizacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'telefono', 'email', 'sector', 'numero_categorias', 'numero_productos', 'numero_atributos', 'numero_imagenes', 'numero_correos', 'seo_avanzado', 'numero_redes_sociales', 'numero_usuarios', 'parametros_desarrollo', 'costo_desarrollo', 'horas_desarrollo', 'parametros_funcionamiento', 'costo_funcionamiento_mensual'], 'required'],
            [['numero_categorias', 'numero_productos', 'numero_atributos', 'numero_imagenes', 'numero_correos', 'seo_avanzado', 'numero_redes_sociales', 'numero_usuarios', 'created_by', 'updated_by'], 'integer'],
            [['costo_desarrollo', 'horas_desarrollo', 'costo_funcionamiento_mensual'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre', 'telefono', 'email', 'sector', 'status'], 'string', 'max' => 255],
            [['parametros_desarrollo', 'parametros_funcionamiento'], 'string', 'max' => 500]
        ];
    }
	
	public function behaviors(){
		return [
			'stripTags' => ['class' => StripTagsBehavior::className(),], 
			'timestamp' => [
	            'class' => TimestampBehavior::className(),
	            'attributes' => [
	                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
	                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
	            ],
	            'value' => new Expression('NOW()'),
             ],
			'blameable' => ['class' => BlameableBehavior::className(),],
        ];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'telefono' => Yii::t('app', 'Telefono'),
            'email' => Yii::t('app', 'Email'),
            'sector' => Yii::t('app', 'Sector'),
            'numero_categorias' => Yii::t('app', 'Categorias'),
            'numero_productos' => Yii::t('app', 'Productos'),
            'numero_atributos' => Yii::t('app', 'Atributos'),
            'numero_imagenes' => Yii::t('app', 'Imagenes'),
            'numero_correos' => Yii::t('app', 'Correos'),
            'seo_avanzado' => Yii::t('app', 'Seo Avanzado'),
            'numero_redes_sociales' => Yii::t('app', 'Redes Sociales'),
            'numero_usuarios' => Yii::t('app', 'Usuarios Diarios'),
            'parametros_desarrollo' => Yii::t('app', 'Parametros Desarrollo'),
            'costo_desarrollo' => Yii::t('app', 'Costo Desarrollo'),
            'horas_desarrollo' => Yii::t('app', 'Horas Desarrollo'),
            'parametros_funcionamiento' => Yii::t('app', 'Parametros Funcionamiento'),
            'costo_funcionamiento_mensual' => Yii::t('app', 'Costo Funcionamiento Mensual'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

	public function procesar(){
		$sd=0;
		foreach($this->desarrollo['q'] as $attribute=>$q){
			$sd+=$this->{$attribute}*$q;
		}
		$this->parametros_desarrollo=Json::encode($this->desarrollo);
		$this->costo_desarrollo=$sd*$this->desarrollo['c'][0];
		$this->horas_desarrollo=$sd*$this->desarrollo['c'][1];
		$sm=0;
		foreach($this->funcionamiento['q'] as $attribute=>$q){
			$sm+=$this->{$attribute}*$q;
		}
		$this->parametros_funcionamiento=Json::encode($this->funcionamiento);
		$this->costo_funcionamiento_mensual=$sm*$this->funcionamiento['c'][0];
	}

    public function sendEmail($email=null)
    {
    	if(is_null($email)){
    		$email=$this->email;
    	}
        return Yii::$app->mailer->compose('cotizacion/creada',['model'=>$this])
            ->setTo($email)
            ->setFrom([Yii::$app->params['adminEmail'] => 'Andrés Fernández'])
            ->setSubject('Cotización Creada | iSeeEC')
            ->send();
    }
}
