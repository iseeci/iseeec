<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
	        'class' => 'yii\web\UrlManager',
	        'showScriptName' => false,
	        'enablePrettyUrl' => true,
	        'rules' => array(
	                '<module:\w+>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
	                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
	                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
	        ),
        ],
    ],
    'modules' => [
        'trx' => [
            'class' => 'frontend\modules\trx\Trx',
        ],
    ],
    'language' => 'es',
    'params' => $params,
];
