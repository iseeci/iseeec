<?php
namespace frontend\components\securitybehaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\Json;
use yii\validators\DateValidator;
use yii\validators\NumberValidator;
use frontend\models\PolizaCertificado;
use frontend\models\Visita;
use frontend\models\VisitaJornada;
use frontend\models\Parametro;

class StripTagsBehavior extends Behavior
{
    public function events()
    {
        return [
            //ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT => 'stripTags',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'stripTags',
            ActiveRecord::EVENT_AFTER_FIND => 'stripTags',
        ];
    }
	
	public function stripTags(){
		foreach($this->owner->attributes as $attribute=>$value){
			if(!is_null($this->owner->{$attribute})){
				$this->owner->{$attribute}=str_replace('\\', '',
											str_replace('/', '',
											str_replace('http:', '',
											str_replace('https:', '', 
											str_replace('..', '', strip_tags($value))))));
			}
		}
	}
}
?>