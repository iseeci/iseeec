<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use cybercog\yii\googleanalytics\widgets\GATracking;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= GATracking::widget(
    [
        'trackingId' => 'UA-68945513-1'
    ]
	) ?>
	<link rel="shortcut icon" href="<?php echo Yii::getAlias('@web/images/site').'/favicon.png'?>" type="image/png" />
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/site/logo-texto-iseeec.png',['alt'=>'iSee E-Commerce','height'=>'30px']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'main-menu navbar-inverse navbar-fixed-top',
        ],
    ]);
	$controller = Yii::$app->controller;
	$default_controller = Yii::$app->defaultRoute;
	$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;
	$isCotizar = (($controller->id === 'cotizacion') && ($controller->action->id === 'crear')) ? true : false;
    $menuItems = [];
    if (Yii::$app->user->isGuest) {
    	$menuItems[] = ['label' => 'Contáctanos', 'url' => ['/site/contact'], 'options'=>['id'=>'link-contacto']];
    	if(!$isCotizar){
    		$menuItems[] = ['label' => 'COTIZAR AHORA', 'url' => ['/trx/cotizacion/crear'], 'options'=>['id'=>'boton-cotizar']];
		} else {
			$menuItems[0]['options']['class']='divider-vertical';
		}
        $menuItems[] = ['label' => 'Signup', 'url' => ['/user/registration/register'], 'options'=>['id'=>'link-sign-up','class'=>'divider-vertical']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/user/security/login']];
    } else {
        $menuItems[] = [
            'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'signup-login navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
	<?php 
		if($isHome || $isCotizar)$containerClass='container-fullpage';
		else $containerClass='container';
	?>
    <div class="<?=$containerClass?>">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="pull-right container-iseeci-footer">
        <p class="pull-right" ><?= Html::a(Html::img('@web/images/site/logo-home-iseeci.png',['alt'=>'iSee Corporate Intelligence | iSeeCI','height'=>'30']),'http://www.iseeci.com/'); ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
