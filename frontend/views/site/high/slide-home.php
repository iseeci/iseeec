<?php
/* @var $this SiteController */
use yii\helpers\Html;
$this->title= 'iSee Electronic Commerce | iSeeEC';
?>
<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "http://www.iseeec.com",
      "logo": "http://www.iseeec.com/images/site/logo-texto-iseeec.png",
      "sameAs" : [
	    "http://www.facebook.com/iseeci",
	    "http://www.twitter.com/iseeci",
	    "http://plus.google.com/+iseeci"
	  ]
    }
</script>
<div id="content">
	<div id="presentacion" style="text-align: center;">
		<div class="home-container row-fluid">
			<div class="container-logo-home col-md-6">
				<?= Html::img('@web/images/site/logo-home-iseeec.png',array('alt'=>'iSee Corporate Intelligence - iSeeCI Business Intelligence Colombia','width'=>'250'));?>	
			</div>
			<div class="container-slogan pull-left">
				<?= \Yii::t('app','ESTRATEGIA').'<br>'.\Yii::t('app','E-COMMERCE').'<br>'.\Yii::t('app','CON VISIÓN').'<br>'; ?>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="home-footer-container row-fluid">
			<div class="container-iseeci pull-right">
				<?= Html::a(Html::img('@web/images/site/logo-home-iseeci.png',['alt'=>'iSee Corporate Intelligence | iSeeCI','height'=>'50']),'http://www.iseeci.com/'); ?>
			</div>
		</div>
	</div>
</div>