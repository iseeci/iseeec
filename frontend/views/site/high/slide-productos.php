<?php 
use yii\bootstrap\Carousel;
$items = [
            ['content' => '<img src="/images/productos/cycle-a.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/productos/cycle-b.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/productos/cycle-c.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/productos/cycle-d.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ];?>
<div class="row">
	<div class="pull-left cycle cycle-productos">
		<?= Carousel::widget(
            [
            	'id'=>'productos',
            	'items' => $items,
            	'options'=>['class'=>'carousel','data-interval'=>'1500']
            ]
            );
         ?>
	</div>
	<div class="pull-left texto texto-productos">
		<p class="subtitulo-1 subtitulo-categorias-1"><?= \Yii::t('app','ARTÍCULOS'); ?></p>
		<p class="subtitulo-2 subtitulo-categorias-2"><?= \Yii::t('app','DISPONIBLES'); ?></p>
		<p class="descripcion descripcion-categorias">
			<?= \Yii::t('app','los productos son aquellos objetos que están disponibles para ser adquiridos en la Tienda en Línea, los que definen su identidad.'); ?>
		</p>
	</div>
</div>
<div class="row">
	<div class="pull-left">
		<div class="titulo titulo-productos">
			<?= \Yii::t('app','PRODUCTOS'); ?>
		</div>
	</div>
</div>
