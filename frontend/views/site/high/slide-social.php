<?php 
use yii\bootstrap\Carousel;
$items = [
            ['content' => '<img src="/images/social/cycle-a.jpg"/>',
            'options'=>['data-slide-number'=>0],
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/social/cycle-b.jpg"/>',
            'options'=>['data-slide-number'=>1],
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/social/cycle-c.jpg"/>',
            'options'=>['data-slide-number'=>2],
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/social/cycle-d.jpg"/>',
            'options'=>['data-slide-number'=>3],
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/social/cycle-e.jpg"/>',
            'options'=>['data-slide-number'=>4],
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/social/cycle-f.jpg"/>',
            'options'=>['data-slide-number'=>5],
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ];?>
<div class="container-social">
	<div class="row-fluid">
		<div class="pull-left cycle cycle-social">
			<?= Carousel::widget(
	            [
	            	'id'=>'social',
	            	'items' => $items,
	            	'options'=>['class'=>'carousel','data-interval'=>'1500']
	            ]
	            );
	         ?>
		</div>
		<div class="pull-left social-buttons-container">
			<div class="text-center">
				<a id="carousel-selector-off-0" style="display:none;"><img src="/images/social/a-off.png" height="35px"/></a>
				<a id="carousel-selector-on-0"><img src="/images/social/a-on.png" height="35px"/></a>
				<a id="carousel-selector-off-1"><img src="/images/social/b-off.png" height="35px"/></a>
				<a id="carousel-selector-on-1" style="display:none;"><img src="/images/social/b-on.png" height="35px"/></a>
				<a id="carousel-selector-off-2"><img src="/images/social/c-off.png" height="35px"/></a>
				<a id="carousel-selector-on-2" style="display:none;"><img src="/images/social/c-on.png" height="35px"/></a>
				<a id="carousel-selector-off-3"><img src="/images/social/d-off.png" height="35px"/></a>
				<a id="carousel-selector-on-3" style="display:none;"><img src="/images/social/d-on.png" height="35px"/></a>
				<a id="carousel-selector-off-4"><img src="/images/social/e-off.png" height="35px"/></a>
				<a id="carousel-selector-on-4" style="display:none;"><img src="/images/social/e-on.png" height="35px"/></a>
				<a id="carousel-selector-off-5"><img src="/images/social/f-off.png" height="35px"/></a>
				<a id="carousel-selector-on-5" style="display:none;"><img src="/images/social/f-on.png" height="35px"/></a>
			</div>
		</div>
		<div class="pull-left texto texto-social">
			<p class="subtitulo-1 subtitulo-social-1"><?= \Yii::t('app','REDES'); ?></p>
			<p class="subtitulo-2 subtitulo-social-2"><?= \Yii::t('app','SOCIALES'); ?></p>
			<p class="descripcion descripcion-social">son la conexión directa de la tienda con los clientes, el medio más eficaz para convertir los clientes potenciales mediante una comunicación selecta y efectiva</p>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row-fluid">
		<div class="pull-left">
			<div class="titulo titulo-social">
				<?= \Yii::t('app','SOCIAL MEDIA'); ?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php
use frontend\assets\SocialAsset;
SocialAsset::register($this);
?>