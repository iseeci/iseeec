<?php 
use yii\bootstrap\Carousel;
$items = [
            ['content' => '<img src="/images/imagenes/cycle-a.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/imagenes/cycle-b.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/imagenes/cycle-c.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/imagenes/cycle-d.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ];?>
<div class="row-fluid">
	<div class="pull-left texto texto-imagenes">
		<p class="subtitulo-1 subtitulo-categorias-1"><?= \Yii::t('app','IMPACTO'); ?></p>
		<p class="subtitulo-2 subtitulo-categorias-2"><?= \Yii::t('app','VISUAL'); ?></p>
		<p class="descripcion descripcion-categorias">
			<?= \Yii::t('app','fotos, diagramas, planos y otras imágenes juegan un papel esencial en la decisión de compra, son aquellas que cautivan el deseo de los usuarios mientras navegan la Tienda Online.'); ?>
		</p>
	</div>
	<div class="pull-left cycle cycle-imagenes">
		<?= Carousel::widget(
            [
            	'id'=>'imagenes',
            	'items' => $items,
            	'options'=>['class'=>'carousel','data-interval'=>'1500']
            ]
            );
         ?>
	</div>
</div>
<div class="row-fluid">
	<div class="pull-right">
		<div class="titulo titulo-imagenes">
			<?= \Yii::t('app','IMÁGENES'); ?>
		</div>
	</div>
</div>
