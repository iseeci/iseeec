<?php 
use yii\bootstrap\Carousel;
$items = [
            ['content' => '<img src="/images/categorias/cycle-a.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/categorias/cycle-b.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/categorias/cycle-c.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/categorias/cycle-d.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ];?>
<div class="row-fluid">
	<div class="pull-left cycle cycle-categorias">
		<?= Carousel::widget(
            [
            	'id'=>'categorias',
            	'items' => $items,
            	'options'=>['class'=>'carousel','data-interval'=>'1500']
            ]
            );
         ?>
	</div>
	<div class="pull-left texto texto-categorias">
		<p class="subtitulo-1 subtitulo-categorias-1"><?= \Yii::t('app','SEGMENTACIÓN'); ?></p>
		<p class="subtitulo-2 subtitulo-categorias-2"><?= \Yii::t('app','DE'); ?> <?= \Yii::t('app','PRODUCTOS'); ?></p>
		<p class="descripcion descripcion-categorias"><?= \Yii::t('app','las categorías son el elemento fundamental de la estructura en la que se clasifican los productos.');?></p>
	</div>
	<div class="clearfix"></div>
</div>
<div class="row-fluid">
	<div class="pull-left">
		<div class="titulo titulo-categorias">
			<?= \Yii::t('app','CATEGORÍAS'); ?>
		</div>
	</div>
</div>
