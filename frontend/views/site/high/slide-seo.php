<div class="row-fluid">
	<div class="pull-left texto texto-seo">
		<p class="subtitulo-1 subtitulo-seo-1"><?= \Yii::t('app','SEARCH ENGINE'); ?></p>
		<p class="subtitulo-2 subtitulo-seo-1"><?= \Yii::t('app','OPTIMIZATION'); ?></p>
		<p class="descripcion descripcion-seo">
			<?= \Yii::t('app','el SEO es el conjunto de actividades destinadas a posicionar la Tienda Online en los primeros lugares de los Motores de Búsqueda como Google, Bing, Yahoo y demás.'); ?> 
			<?= \Yii::t('app','de esta manera se conecta la Tienda Virtual con las palabras específicas que están buscando los clientes.'); ?>
		</p>
	</div>
</div>
<div class="row-fluid">
	<div class="pull-right">
		<div class="titulo titulo-seo">
			<?= \Yii::t('app','S.E.O'); ?>
		</div>
	</div>
</div>