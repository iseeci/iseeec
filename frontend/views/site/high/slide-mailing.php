<div class="row-fluid">
	<div class="pull-left texto texto-mailing">
		<p class="subtitulo-1 subtitulo-mailing-1"><?= \Yii::t('app','CORREO'); ?></p>
		<p class="subtitulo-2 subtitulo-mailing-2"><?= \Yii::t('app','ELECTRÓNICO'); ?></p>
		<p class="descripcion descripcion-mailing-1">
			<?= \Yii::t('app','el envío de información no puede ser destinado a publicidad masiva e impersonal, sino a comunicaciones directas y personalizadas.').'<br>'.\Yii::t('app','se trata del canal más apropiado para generar impacto a través del lenguaje,'); ?>
		</p>
	</div>
	<div class="pull-left texto texto-mailing texto-mailing-2">
		<p class="descripcion descripcion-mailing"><?= \Yii::t('app','responder correctamente inquietudes, construir la relación personalizada con el cliente y ofrecer información actualizada relacionada con la Tienda Online.'); ?></p>
	</div>
</div>
<div class="row-fluid">
	<div class="pull-left">
		<div class="titulo titulo-mailing">
			<?= \Yii::t('app','MAILING'); ?>
		</div>
	</div>
</div>