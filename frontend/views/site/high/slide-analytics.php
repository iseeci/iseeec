<div class="row-fluid">
	<div class="pull-right texto texto-analytics">
		<p class="subtitulo-1 subtitulo-analytics-1"><?= \Yii::t('app','ESTADÍSTICAS'); ?></p>
		<p class="subtitulo-2 subtitulo-analytics-1"><?= \Yii::t('app','APLICABLES'); ?></p>
		<p class="descripcion descripcion-analytics">
			<?= \Yii::t('app','Google Analytics es la principal fuente de información del comportamiento de los usuarios; son registros que permiten identificar datos geográficos, demográficos y tendencias de compra del consumidor, además de su interacción con la Tienda.'); ?> 
		</p>
	</div>
	<div class="clearfix"></div>
</div>
<div class="row-fluid">
	<div class="pull-right">
		<div class="titulo titulo-analytics">
			<?= \Yii::t('app','ANALYTICS'); ?>
		</div>
	</div>
</div>