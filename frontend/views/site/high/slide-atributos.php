<?php 
use yii\bootstrap\Carousel;
$items = [
            ['content' => '<img src="/images/atributos/cycle-a.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/atributos/cycle-b.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/atributos/cycle-c.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ['content' => '<img src="/images/atributos/cycle-d.jpg"/>',
            //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            ],
            ];?>
<div class="row-fluid">
	<div class="hidden-sm pull-left cycle cycle-atributos">
		<?= Carousel::widget(
            [
            	'id'=>'atributos',
            	'items' => $items,
            	'options'=>['class'=>'carousel','data-interval'=>'1500']
            ]
            );
         ?>
	</div>
	<div class="hidden-sm col-lg-1">
	</div>
	<div class="pull-left texto texto-atributos">
		<p class="subtitulo-1 subtitulo-atributos-1"><?= \Yii::t('app','CARACTERIZACIÓN'); ?></p>
		<p class="subtitulo-2 subtitulo-atributos-2"><?= \Yii::t('app','DE'); ?> <?= \Yii::t('app','PRODUCTOS'); ?></p>
		<p class="descripcion descripcion-atributos"><?= \Yii::t('app','nos permiten definir distintas opciones de compra para un mismo producto: talla, color, tamaño, marca, fabricante, país...'); ?></p>
	</div>
</div>
<div class="row-fluid">
	<div class="pull-left">
		<div class="titulo titulo-atributos">
			<?= \Yii::t('app','ATRIBUTOS'); ?>
		</div>
	</div>
</div>
