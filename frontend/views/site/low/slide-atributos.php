<div class="row-fluid">
	<div class="texto-low texto-atributos-low">
		<p class="subtitulo-1-low subtitulo-atributos-1-low"><?= \Yii::t('app','CARACTERIZACIÓN'); ?></p>
		<p class="subtitulo-2-low subtitulo-atributos-2-low"><?= \Yii::t('app','DE'); ?> <?= \Yii::t('app','PRODUCTOS'); ?></p>
		<p class="descripcion-low descripcion-atributos-low"><?= \Yii::t('app','nos permiten definir distintas opciones de compra para un mismo producto: talla, color, tamaño, marca, fabricante, país...'); ?></p>
	</div>
</div>
<div class="row-fluid">
	<div class="titulo-low titulo-atributos-low">
		<?= \Yii::t('app','ATRIBUTOS'); ?>
	</div>
</div>
