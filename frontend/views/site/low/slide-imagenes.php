<div class="row-fluid">
	<div class="texto-low texto-imagenes-low">
		<p class="subtitulo-1-low subtitulo-categorias-1-low"><?= \Yii::t('app','IMPACTO'); ?></p>
		<p class="subtitulo-2-low subtitulo-categorias-2-low"><?= \Yii::t('app','VISUAL'); ?></p>
		<p class="descripcion-low descripcion-categorias-low">
			<?= \Yii::t('app','fotos, diagramas, planos y otras imágenes juegan un papel esencial en la decisión de compra, son aquellas que cautivan el deseo de los usuarios mientras navegan la Tienda Online.'); ?>
		</p>
	</div>
</div>
<div class="row-fluid">
	<div class="titulo-low titulo-imagenes-low">
		<?= \Yii::t('app','IMÁGENES'); ?>
	</div>
</div>
