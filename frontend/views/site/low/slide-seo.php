<div class="row-fluid">
	<div class="pull-left texto-low texto-seo-low">
		<p class="subtitulo-1-low subtitulo-seo-1-low"><?= \Yii::t('app','SEARCH ENGINE'); ?></p>
		<p class="subtitulo-2-low subtitulo-seo-1-low"><?= \Yii::t('app','OPTIMIZATION'); ?></p>
		<p class="descripcion-low descripcion-seo-low">
			<?= \Yii::t('app','el SEO es el conjunto de actividades destinadas a posicionar la Tienda Online en los primeros lugares de los Motores de Búsqueda como Google, Bing, Yahoo y demás.'); ?><br>
			<?= \Yii::t('app','de esta manera se conecta la Tienda Virtual con las palabras específicas que están buscando los clientes.'); ?>
		</p>
	</div>
</div>
<div class="row-fluid">
	<div class="titulo-low titulo-seo-low">
		<?= \Yii::t('app','S.E.O'); ?>
	</div>
</div>