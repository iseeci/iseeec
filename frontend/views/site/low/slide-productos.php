<div class="row-fluid">
	<div class="texto-low texto-productos-low">
		<p class="subtitulo-1-low subtitulo-categorias-1-low"><?= \Yii::t('app','ARTÍCULOS'); ?></p>
		<p class="subtitulo-2-low subtitulo-categorias-2-low"><?= \Yii::t('app','DISPONIBLES'); ?></p>
		<p class="descripcion-low descripcion-categorias-low">
			<?= \Yii::t('app','los productos son aquellos objetos que están disponibles para ser adquiridos en la Tienda en Línea, los que definen su identidad.'); ?>
		</p>
	</div>
</div>
<div class="row-fluid">
	<div class="titulo-low titulo-productos-low">
		<?= \Yii::t('app','PRODUCTOS'); ?>
	</div>
</div>
