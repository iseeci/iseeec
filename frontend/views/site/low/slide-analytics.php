<div class="row-fluid">
	<div class="texto-low texto-analytics-low">
		<p class="subtitulo-1-low subtitulo-analytics-1-low"><?= \Yii::t('app','ESTADÍSTICAS'); ?></p>
		<p class="subtitulo-2-low subtitulo-analytics-1-low"><?= \Yii::t('app','APLICABLES'); ?></p>
		<p class="descripcion-low descripcion-analytics-low">
			<?= \Yii::t('app','Google Analytics es la principal fuente de información del comportamiento de los usuarios; son registros que permiten identificar datos geográficos, demográficos y tendencias de compra del consumidor, además de su interacción con la Tienda.'); ?> 
		</p>
	</div>
	<div class="clearfix"></div>
</div>
<div class="row-fluid">
	<div class="titulo-low titulo-analytics-low">
		<?= \Yii::t('app','ANALYTICS'); ?>
	</div>
</div>