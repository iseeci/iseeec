<div class="row-fluid">
	<div class="texto-low texto-categorias-low">
		<p class="subtitulo-1-low subtitulo-categorias-1-low"><?= \Yii::t('app','SEGMENTACIÓN'); ?></p>
		<p class="subtitulo-2-low subtitulo-categorias-2-low"><?= \Yii::t('app','DE'); ?> <?= \Yii::t('app','PRODUCTOS'); ?></p>
		<p class="descripcion-low descripcion-categorias-low"><?= \Yii::t('app','las categorías son el elemento fundamental de la estructura en la que se clasifican los productos.');?></p>
	</div>
</div>
<div class="row-fluid">
		<div class="titulo-low titulo-categorias-low">
			<?= \Yii::t('app','CATEGORÍAS'); ?>
		</div>
</div>
