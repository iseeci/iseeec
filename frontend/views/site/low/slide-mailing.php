<div class="row-fluid">
	<div class="texto-low texto-mailing-low">
		<p class="subtitulo-1-low subtitulo-mailing-1-low"><?= \Yii::t('app','CORREO'); ?></p>
		<p class="subtitulo-2-low subtitulo-mailing-2-low"><?= \Yii::t('app','ELECTRÓNICO'); ?></p>
		<p class="descripcion-low descripcion-mailing-low"><?= \Yii::t('app',' se trata del canal más apropiado para generar impacto a través del lenguaje, responder correctamente inquietudes, construir la relación personalizada con el cliente y ofrecer información actualizada relacionada con la Tienda Online.'); ?></p>
		<?php /* <p class="descripcion-low descripcion-mailing-1-low"><?= \Yii::t('app','el envío de información actualmente no puede ser destinado a publicidad masiva e impersonal, sino a comunicaciones directas y personalizadas.'); ?></p> */ ?>
	</div>
</div>
<div class="row-fluid">
	<div class="titulo-low titulo-mailing-low">
		<?= \Yii::t('app','MAILING'); ?>
	</div>
</div>