<div class="row-fluid">
	<div class="texto-low texto-social-low">
		<p class="subtitulo-1-low subtitulo-social-1-low"><?= \Yii::t('app','REDES'); ?></p>
		<p class="subtitulo-2-low subtitulo-social-2-low"><?= \Yii::t('app','SOCIALES'); ?></p>
		<p class="descripcion-low descripcion-social-low">son la conexión directa de la tienda con los clientes, el medio más eficaz para convertir los clientes potenciales mediante una comunicación selecta y efectiva</p>
	</div>
</div>
<div class="row-fluid">
	<div class="titulo-low titulo-social-low">
		<?= \Yii::t('app','SOCIAL MEDIA'); ?>
	</div>
</div>