<?php
use yii\web\View;
/* @var $this yii\web\View */

$this->title = 'iSee E-Commerce | iSeeEC';
$this->registerMetaTag(['content'=>\Yii::t('app','e-commerce, iseeec, iseeci, small, medium, stores'),'name'=>'keywords']);
$this->registerMetaTag(['content'=>\Yii::t('app','E-Commerce Platform for small and medium-sized online stores'),'name'=>'description']);
?>
<div class="site-index">
    <div id="fullpage" class="fullpage-wrapper">
	    <div id="home-section" class="section" data-anchored="home">
	    	<div class="iseeec-slide slide-home">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-home'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-home'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="categorias-section" class="section" data-anchored="categorias">
	    	<div class="iseeec-slide slide-categorias">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-categorias'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-categorias'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="productos-section" class="section" data-anchored="productos">
	    	<div class="iseeec-slide slide-productos">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-productos'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-productos'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="atributos-section" class="section" data-anchored="atributos">
	    	<div class="iseeec-slide slide-atributos">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-atributos'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-atributos'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="imagenes-section" class="section" data-anchored="imagenes">
	    	<div class="iseeec-slide slide-imagenes">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-imagenes'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-imagenes'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="social-section" class="section" data-anchored="social">
	    	<div class="iseeec-slide slide-social">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-social'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-social'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="mailing-section" class="section" data-anchored="mailing">
	    	<div class="iseeec-slide slide-mailing">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-mailing'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-mailing'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="seo-section" class="section" data-anchored="seo">
	    	<div class="iseeec-slide slide-seo">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-seo'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-seo'); ?>
	    		</div>
	    	</div>
	    </div>
	    <div id="analytics-section" class="section" data-anchored="analytics">
	    	<div class="iseeec-slide slide-analytics">
	    		<div class="high-res hidden-xs hidden-sm">
	    			<?= $this->render('high/slide-analytics'); ?>
	    		</div>
	    		<div class="low-res hidden-md hidden-lg">
	    			<?= $this->render('low/slide-analytics'); ?>
	    		</div>
	    	</div>
	    </div>
	</div>
	
    <?php 
    	xj\js\fullpage\FullpageAsset::register($this);
    	$this->registerJs("$(document).ready(function() {
			$('#fullpage').fullpage();
		});", View::POS_READY, 'full-page-init');
	?>
</div>
