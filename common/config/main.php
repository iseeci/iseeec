<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
	    'mobileDetect' => [
	        'class' => '\ezze\yii2\mobiledetect\MobileDetect'
	    ],
    ],
    'modules' => [
	    'user' => [
	        'class' => 'dektrium\user\Module',
	    ],
	    'rbac' => [
	        'class' => 'dektrium\rbac\Module',
	    ],
    ],
    'language' => 'es',
];
